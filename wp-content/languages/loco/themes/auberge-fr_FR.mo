��                �      �  4   �  $   2     W  +   m  	   �     �     �  2   �  [   �     Q  (   o     �  M   �  N   �     J     \  	   r     |  !   �  )   �  7   �               3  "   N     q  \   ~  	   �     �  �  �     �     �     �      	     2	     D	     Q	  "   e	  P   �	     �	  
   �	     �	     
     
     &
     8
     U
     e
     v
     �
  J   �
     �
        
   #  (   .     W  E   m  	   �  	   �   %s: Name of current post.Continue reading%s&hellip; %s: number of comments.Comments: %s &larr; Older comments Add new comment link text.Add yours &rarr; All posts Back to top Back to top &uarr; Comments are closed. You can not add new comments. Comments list title.%1$d comment on &ldquo;%2$s&rdquo; %1$d comments on &ldquo;%2$s&rdquo; Contact support center &rarr; Display search form button title.Search Edit post link.Edit It looks like nothing was found at this location. Maybe try the search below? It seems we can not find what you are looking for. Perhaps searching can help. Load more&hellip; Newer comments &rarr; Next post Nothing Found Oops! That page can not be found. Pagination text (hidden): next.Next page Please contact the administrator of this site for help. Previous post Search Results for: %s Search field label.Search Search field: type and press enter Show Details Sorry, but nothing matched your search terms. Please try again with some different keywords. Subscript Welcome Project-Id-Version: Auberge
Report-Msgid-Bugs-To: https://support.webmandesign.eu
POT-Creation-Date: 2019-04-17 17:29+0200
PO-Revision-Date: 2019-05-13 21:09+0000
Last-Translator: admin <maxime.chofflet.simplon@gmail.com>
Language-Team: Français
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=2; plural=n > 1;
X-Poedit-SearchPath-0: .
X-Loco-Version: 2.2.2; wp-5.2 Continuer la lecture%s&hellip; Commentaires: %s &larr; Anciens commentaires Ajouter votre commentaire &rarr; Tous les articles Haut de page Haut de page &uarr; Les commentaires sont désactivés %1$d commentaire sur &ldquo;%2$s&rdquo; %1$d commentaires sur &ldquo;%2$s&rdquo; Contact support &rarr; Rechercher Modifier Aucun résultat. Aucun résultat. Voir plus&hellip; Commentaires récents &rarr; Article suivant Aucun résultat. Oops! Cette page n'existe pas. Page suivante Merci de contacter l'administrateur de ce site si vous avez besoin d'aide. Article précédent Recherche de résultats pour: %s Rechercher Votre recherche puis appuyez sur entrée Afficher les détails Désolé nous n'avons rien trouvé correspondant à votre recherche.  Souscrire Bienvenue 