<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'wordpress' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'WordPress' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', 'To#WwJaRi2Mf5-' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Zo{pZ]Qk9s.$w/7T`a!VJl#Zb/Yt]@>1Rg]0i~tHSF.`H] pscc%xBoUr;O]Ar)g' );
define( 'SECURE_AUTH_KEY',  '4~9WKYp-!0gg=`4lu?r5UY`pB:=)udsb_0ZjGxsi?Z|/ya{xaQr9WFqi90@n!GMN' );
define( 'LOGGED_IN_KEY',    'B1y>>;imPOA:L?Q7%E}*}Q!`QZZ-MY6<?C|&KW7=_rRKcw{v: Fv!,qKmL7+hJi/' );
define( 'NONCE_KEY',        '}WfYo|,|l(.fgh*9mmf>*@[I?nXkMF0Hj)/WiEy0@`#!B-V[]Ogoxqb6!b(OE&ZH' );
define( 'AUTH_SALT',        'UezM% -Ki%Q(]42oZ(o9Q>OxP#S@rXtVeDH>_8yVJ>{al|ES~01bRT$BSXD%`H9<' );
define( 'SECURE_AUTH_SALT', 'fyuUE$<Yu0{]P;{qC_-)};lrc}1JZNK/5<Bi6HiZ/| jhOG1MmAeM])&,tqvq|&[' );
define( 'LOGGED_IN_SALT',   '$4 LBPp-mFDs!(a>I=YF`I26Ni^v_gTqM7hd|~(O,#bjZt6}mtH_ymI><c}G()]!' );
define( 'NONCE_SALT',       'ki#a[$VMycmu3p_BsbYFz6%IK7Ds6UhycYMaK/#2BeF+m>/QUfOj343Z|Co~$v)y' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');

define('WP_MEMORY_LIMIT', '128M');

